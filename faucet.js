const express = require("express");
const bodyParser = require('body-parser');
const {body, validationResult} = require('express-validator');
const {sendFaucet} = require("./src/send")
const {verificationSend, setRestrictions} = require("./src/verification")

const app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(bodyParser.raw());

const allowCrossDomain = function (req, res, next) {
    if (req.get('Origin')) {
        res.setHeader("Access-Control-Allow-Origin", req.get('Origin'));
        res.setHeader("Access-Control-Allow-Credentials", "true");
        res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
        res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }

    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
        res.send(200);
    } else {
        next();
    }
};
app.use(allowCrossDomain);

app.listen(3000, () => {
    console.log("Server running on port 3000");
});

app.post(
    "/getFaucet",
    body('address').isLength({min: 30}),
    body('recaptcha').isString(),
    body('browser').isString(),
    async (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({errors: errors.array()});
        }

        if (!(await verificationSend(req.headers['x-real-ip'] || req.socket.remoteAddress, req.body.browser, req.body.recaptcha))) {
            return res.status(422).json({errors: [{'param': 'limit'}]});
        }

        if (await sendFaucet(req.body.address)) {
            await setRestrictions(req.socket.remoteAddress, req.body.browser);
        }

        res.json({status: true});
    });


