const { createClient } = require('redis');

exports.getRedisClient = (async () => {
    const client = createClient({
        url: 'redis://blockchain_redis:6379',
    });

    client.on('error', (err) => console.log('Redis Client Error', err));

    await client.connect();

    return client;
})();
