const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));

exports.sendFaucet = async function (address) {
    let body = {
        jsonrpc: "2.0",
        id: 1,
        method: "tx_send",
        params: {
            "value": 2500000000,
            "fee": 1000000,
            "address": address,
            "comment": "send faucet",
            "asset_id": 0,
            "offline": false
        }
    };

    const response = await fetch('http://blockchain_wallet_api:10001/api/wallet', {
        method: 'post',
        body: JSON.stringify(body),
        headers: {'Content-Type': 'application/json'}
    });

    if (response.ok) {
        const data = await response.json();
        console.log(data);

        return true;
    }

    return false;
}
