const {getRedisClient} = require("./redis")
const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));

let redis = null;

exports.verificationSend = async function (remoteIp, browser, recaptcha) {
    redis = await getRedisClient;
    let status = true;

    if (!await validateRecaptcha(recaptcha)) {
        console.log('limit recaptcha');

        status = false;
    }
    if (await checkRestriction(getKey('ip', remoteIp))) {
        console.log('limit ip');

        status = false;
    } else if (await checkRestriction(getKey('browser', browser))) {
        console.log('limit browser');

        status = false;
    }

    return status;
}

exports.setRestrictions = async function (remoteIp, browser) {
    await setRestriction(getKey('ip', remoteIp));
    await setRestriction(getKey('browser', browser));
}

async function checkRestriction(key) {
    return !!(await redis.get(key));
}

async function setRestriction(key) {
    await redis.set(key, 'true');
    await redis.expire(key, 86400);
}

function getKey(type, key) {
    return `restriction_by_${type}_${key}`;
}

async function validateRecaptcha(token) {
    const secret_key = process.env.RECAPTCHAV3_SECRET_KEY;

    const response = await fetch(`https://www.google.com/recaptcha/api/siteverify?secret=${secret_key}&response=${token}`, {
        method: 'post',
    });

    if (!response.ok) {
        return false;
    }

    const json = await response.json();

    return !(!json.success || json.score < 0.5);
}
